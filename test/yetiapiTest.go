package main

import (
	"context"
	"fmt"
	"log"
	"time"
	proto "yeti-grpc/protobuf"

	"github.com/golang/protobuf/ptypes"
	"google.golang.org/grpc"
)

const (
	address     = "localhost:50051"
	defaultName = "goblin"
)

func main() {
	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := proto.NewYetiClient(conn)

	// Contact the server and print out its response.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	v, _ := c.ShowVersion(ctx, nil)
	fmt.Println("version: ", v)
	/*
		dp := proto.ExistingDialpeer{Id: "2309253"}
		fmt.Println(&dp)
		r, err := c.GetDialpeerByID(ctx, &dp)
		if err != nil {
			log.Fatalf("could not greet: %v", err)
		}
		log.Println(r)*/

	/*
		filter := proto.Filters{Prefix: "7495"}
		stream, err := c.GetDialpeerByFilter(ctx, &filter)
		if err != nil {
			log.Println("Stream cannot be created: ", err)
		}

		for {
			feature, err := stream.Recv()
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatalf("%v.ListFeatures(_) = _, %v", c, err)
			}
			log.Println(feature)
		}*/

	activate, err := ptypes.TimestampProto(time.Now())
	if err != nil {
		fmt.Println("can't conver time to timestamp")
	}

	deactivate, err := ptypes.TimestampProto(time.Now().AddDate(5, 0, 0))
	if err != nil {
		fmt.Println("can't conver time to timestamp")
	}

	d := proto.Dialpeer{
		Enabled:         true,
		ConnectFee:      0.0,
		InitialInterval: 1,
		InitialRate:     0.0,
		NextInterval:    1,
		NextRate:        0.0,
		ValidFrom:       activate,
		ValidTill:       deactivate,
		RoutingGroup:    24,
		Vendor:          70,
		Account:         69,
		GatewayGroup:    6,
		Capacity:        1,
		Prefix:          "79999999999",
	}

	q, e := c.CreateDialpeer(ctx, &d)
	if e != nil {
		fmt.Println(e)
	}
	fmt.Println(q)
}
