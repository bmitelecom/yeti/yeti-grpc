package yetiapi

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"net/url"
	"strconv"
	"strings"

	"github.com/golang/protobuf/jsonpb"
	proto "gitlab.com/bmitelecom/yeti/yeti-api-protobuf"
	"golang.org/x/net/context"
)

//AuthResponse represents a Kazoo authentication record
type AuthResponse struct {
	AuthToken string `json:"jwt"`
}

// Linger please
var (
	_ context.Context
)

type DialpeerApiService service

//GetDialpperByID returns parameters of a particular dialpeer
func (a *DialpeerApiService) GetDialpeerByID(ctx context.Context, id string) (*proto.ExistingDialpeer, error) {
	localVarHttpMethod := "GET"

	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/dialpeers/" + id

	r, err := a.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := a.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode >= 300 {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	//bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
	//fmt.Println(fmt.Print(bodyBytes))

	/*
			{"data":{"id":"2309253","type":"dialpeers","links":{"self":"http://10.8.1.18/api/rest/admin/dialpeers/2309253"},"attributes":{"enabled":true,"next-rate":"0.0","connect-fee":"0.0","initial-rate":"0.0","initial-interval":1,"next-interval":1,"valid-from":"2016-09-01T04:15:00.000+00:00","valid-till":"2022-06-06T12:20:00.000+00:00","prefix":"78005052250","src-rewrite-rule":"","dst-rewrite-rule":"","acd-limit":0.0,"asr-limit":0.0,"src-rewrite-result":"","dst-rewrite-result":"","locked":false,"priority":100,"exclusive-route":true,"capacity":2,"lcr-rate-multiplier":"1.0","force-hit-rate":null,"network-prefix-id":34729,"created-at":"2018-07-07T09:

		T 2018/07/09 03:25:07.708527 10.8.1.18:6666 -> 10.8.0.2:62159 [AP]
		08:42.465+00:00","short-calls-limit":1.0,"external-id":null,"routing-tag-ids":[]},"relationships":{"gateway":{"links":{"self":"http://10.8.1.18/api/rest/admin/dialpeers/2309253/relationships/gateway","related":"http://10.8.1.18/api/rest/admin/dialpeers/2309253/gateway"}},"gateway-group":{"links":{"self":"http://10.8.1.18/api/rest/admin/dialpeers/2309253/relationships/gateway-group","related":"http://10.8.1.18/api/rest/admin/dialpeers/2309253/gateway-group"}},"routing-group":{"links":{"self":"http://10.8.1.18/api/rest/admin/dialpeers/2309253/relationships/routing-group","related":"http://10.8.1.18/api/rest/admin/dialpeers/2309253/routing-group"}},"vendor":{"links":{"self":"http://10.8.1.18/api/rest/admin/dialpeers/2309253/relationships/vendor","related":"http://10.8.1.18/api/rest/admin/dialpeers/2309253/vendor"}},"account":{"links":{"self":"http://10.8.1.18/api/rest/admin/dialpeers/2309253/relationships/account","related":"http://10.8.1.18/api/rest/admin/dialpeers/2309253/account"}},"dialpeer-next-rates":{"links":{"self":"http://10.8.1.18/api/rest/admin/dialpeers/2309253/relationships/dialpeer-next-rates","related":"http://10.8.1.18/api/rest/admin/dialpeers/2309253/dialpeer-next-rates"}}}}}.

	*/

	de := proto.ExistingDataEnvelope{}

	decoder := json.NewDecoder(localVarHttpResponse.Body)

	um := jsonpb.Unmarshaler{AllowUnknownFields: true}
	umErr := um.UnmarshalNext(decoder, &de)
	if umErr != nil {
		return nil, umErr
	}

	dp := de.Data

	return dp, err
}

func decodeDialpeer(body io.Reader) (dp *proto.ExistingDialpeer, err error) {
	de := proto.ExistingDataEnvelope{}

	decoder := json.NewDecoder(body)

	um := jsonpb.Unmarshaler{AllowUnknownFields: true}
	err = um.UnmarshalNext(decoder, &de)
	if err != nil {
		return nil, err
	}

	dp = de.Data
	return dp, nil
}

//ListDialpeersByFilter returns list of dialpeers using applied filter (up to 3)
func (a *DialpeerApiService) ListDialpeersByFilter(ctx context.Context, filter *proto.Filters) ([]*proto.ExistingDialpeer, error) {
	localVarHttpMethod := "GET"
	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/dialpeers?"

	params := make([]string, 3)

	if filter.Prefix != "" {
		params = append(params, "filter%5Bprefix%5D="+filter.Prefix)
	}

	if filter.ExternalId != "" {
		params = append(params, "filter%5Bexternal_id%5D="+filter.ExternalId)
	}

	if filter.RoutingGroupId != "" {
		params = append(params, "filter%5Brouting_group_id%5D="+filter.RoutingGroupId)
	}

	localVarPath += localVarPath + strings.Join(params, "&")

	r, err := a.client.simpleRequest(ctx, localVarPath, localVarHttpMethod)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := a.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode >= 300 {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	//bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
	//fmt.Println(fmt.Print(bodyBytes))

	de := proto.ListOfExistingDialpeers{}

	decoder := json.NewDecoder(localVarHttpResponse.Body)

	um := jsonpb.Unmarshaler{AllowUnknownFields: true}
	umErr := um.UnmarshalNext(decoder, &de)
	if umErr != nil {
		return nil, umErr
	}

	listDp := de.Data

	return listDp, err
}

func checkFloat(param string) bool {
	_, convErr := strconv.ParseFloat(param, 32)
	if convErr != nil {
		return false
	}
	return true
}

/* According to the official documentation,
we have to pass sufficient data for a dialpeer creation
https://demo.yeti-switch.org/doc/api/admin/dialpeers/create_new_entry.html
*/

func (a *DialpeerApiService) CreateDialpeer(ctx context.Context, dp *proto.Dialpeer) (edp *proto.ExistingDialpeer, err error) {
	var (
		localVarFileName  string
		localVarFileBytes []byte
		//localJSONBuffer   bytes.Buffer
	)

	//Do some sanity checks of data

	if dp.RoutingGroup == 0 || dp.Vendor == 0 || dp.Account == 0 {
		return nil, reportError("You have to specify all required parameters")
	}

	if dp.InitialInterval == 0 || dp.NextInterval == 0 || dp.Capacity == 0 {
		return nil, reportError("InitialInterval, NextInterval, Capacity must be greater than zero")
	}

	//activate := time.Now()                  //Activate a dialpeer since the moment of creation
	//deactivate := activate.AddDate(5, 0, 0) //Deactivate a dialpeer after 5 years

	attrs := proto.DialpeerAttributes{
		Enabled:           dp.Enabled,
		ConnectFee:        dp.ConnectFee,
		InitialInterval:   1,
		InitialRate:       dp.InitialRate,
		NextInterval:      1,
		NextRate:          dp.NextRate,
		ValidFrom:         dp.ValidFrom,
		ValidTill:         dp.ValidTill,
		SrcRewriteRule:    dp.SrcRewriteRule,
		SrcRewriteResult:  dp.SrcRewriteResult,
		DstRewriteRule:    dp.DstRewriteRule,
		DstRewriteResult:  dp.DstRewriteResult,
		AcdLimit:          dp.AcdLimit,
		AsrLimit:          dp.AsrLimit,
		Locked:            dp.Locked,
		ExclusiveRoute:    dp.ExclusiveRoute,
		Capacity:          dp.Capacity,
		ForceHitRate:      dp.ForceHitRate,
		NetworkPrefixId:   dp.NetworkPrefixId,
		CreatedAt:         dp.CreatedAt,
		ExternalId:        dp.ExternalId,
		RoutingTagIds:     dp.RoutingTagIds,
		LcrRateMultiplier: 1.0,
		Priority:          100,
		ShortCallsLimit:   1,
	}

	if dp.Prefix != "" {
		attrs.Prefix = dp.Prefix
	}

	if dp.LcrRateMultiplier > 0 {
		attrs.LcrRateMultiplier = dp.LcrRateMultiplier
	}

	if dp.Priority != 100 {
		attrs.Priority = dp.Priority
	}

	if dp.InitialInterval > 0 {
		attrs.InitialInterval = dp.InitialInterval
	}

	if dp.NextInterval > 0 {
		attrs.NextInterval = dp.NextInterval
	}

	if dp.ShortCallsLimit > 0 {
		attrs.ShortCallsLimit = dp.ShortCallsLimit
	}

	rels := proto.NewDialpeerRelationships{
		Vendor:       &proto.Relation{Data: &proto.RelationData{Type: "contractors", Id: dp.Vendor}},
		Account:      &proto.Relation{Data: &proto.RelationData{Type: "accounts", Id: dp.Account}},
		RoutingGroup: &proto.Relation{Data: &proto.RelationData{Type: "routing-groups", Id: dp.RoutingGroup}},
		//RoutingTagModes: &proto.Relation{Data: &proto.RelationData{Type: "routing-tag-modes", Id: dp.RoutingTagModes}},
	}

	if dp.Gateway > 0 {
		rels.Gateway = &proto.Relation{Data: &proto.RelationData{Type: "gateways", Id: dp.Gateway}}
	}

	if dp.GatewayGroup > 0 {
		rels.GatewayGroup = &proto.Relation{Data: &proto.RelationData{Type: "gateway-groups", Id: dp.GatewayGroup}}
	}

	dialpeer := proto.NewDialpeer{
		Type:          "dialpeers",
		Attributes:    &attrs,
		Relationships: &rels,
	}

	data := proto.NewDataEnvelope{Data: &dialpeer}

	marshaller := jsonpb.Marshaler{
		EmitDefaults: true,
	}
	jsonStr, marErr := marshaller.MarshalToString(&data)
	//marErr := marshaller.Marshal(localJSONBuffer, &data)
	if marErr != nil {
		return nil, marErr
	}

	localVarHttpMethod := "POST"
	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/dialpeers"

	//localVarPostBody, bodyErr := setBody(localJSONBuffer, "json")
	localVarPostBody, bodyErr := setBody(jsonStr, "json")
	if bodyErr != nil {
		return nil, bodyErr
	}

	// create path and map variables
	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	localVarHeaderParams["Content-Type"] = "application/vnd.api+json"

	// body params
	//localVarPostBody = &authBody
	r, err := a.client.prepareRequest(
		ctx,
		localVarPath,
		localVarHttpMethod,
		localVarPostBody,
		localVarHeaderParams,
		localVarQueryParams,
		localVarFormParams,
		localVarFileName,
		localVarFileBytes)

	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := a.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode >= 300 {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	if localVarHttpResponse.StatusCode == 201 {
		edp, err = decodeDialpeer(localVarHttpResponse.Body)
		if err != nil {
			return nil, err
		}
	}

	return edp, nil

}

func (a *DialpeerApiService) UpdateDialpeer(ctx context.Context, dp *proto.ExistingDialpeer) (edp *proto.ExistingDialpeer, err error) {
	var (
		localVarFileName  string
		localVarFileBytes []byte
		//localJSONBuffer   bytes.Buffer
	)

	dp.Type = "dialpeers"

	data := proto.ExistingDataEnvelope{Data: dp}

	marshaller := jsonpb.Marshaler{
		EmitDefaults: false,
	}
	jsonStr, marErr := marshaller.MarshalToString(&data)
	//marErr := marshaller.Marshal(localJSONBuffer, &data)
	if marErr != nil {
		return nil, marErr
	}

	localVarHttpMethod := "PUT"
	// create path and map variables
	localVarPath := a.client.cfg.BasePath + "/dialpeers/" + dp.Id

	//localVarPostBody, bodyErr := setBody(localJSONBuffer, "json")
	localVarPostBody, bodyErr := setBody(jsonStr, "json")
	if bodyErr != nil {
		return nil, bodyErr
	}

	// create path and map variables
	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	localVarHeaderParams["Content-Type"] = "application/vnd.api+json"

	// body params
	//localVarPostBody = &authBody
	r, err := a.client.prepareRequest(
		ctx,
		localVarPath,
		localVarHttpMethod,
		localVarPostBody,
		localVarHeaderParams,
		localVarQueryParams,
		localVarFormParams,
		localVarFileName,
		localVarFileBytes)

	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := a.client.callAPI(ctx, r)
	if err != nil || localVarHttpResponse == nil {
		return nil, err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode >= 300 {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	if localVarHttpResponse.StatusCode == 200 {
		edp, err = decodeDialpeer(localVarHttpResponse.Body)
		if err != nil {
			return nil, err
		}
	}

	return edp, nil

}
