package yetiapi

import (
	"context"
	"io/ioutil"
	"math"
	"net/http"
	"net/url"
)

/*
{
    "data": {
        "account": {
            "per_minute": {
                "registrations": 100,
                "invites": 100,
                "total_packets": 1000
            },
            "per_second": {
                "registrations": 5,
                "invites": 5,
                "total_packets": 20
            }
        },
        "device": {
            "per_minute": {
                "registrations": 10,
                "invites": 10,
                "total_packets": 100
            },
            "per_second": {
                "registrations": 2,
                "invites": 4,
                "total_packets": 10
            }
        }
    }
}
*/

type RateLimiterService service

//RateLimitParameters holds main options for every type
//of rate limits
type RateLimitParameters struct {
	Registrations int16 `json:"registrations"`
	Invites       int16 `json:"invites"`
	TotalPackets  int16 `json:"total_packets"`
}

//RateLimitTypes holds types of rate-limits
//it has to be 2 type as Frontier require
type RateLimitTypes struct {
	PerMinute RateLimitParameters `json:"per_minute"`
	PerSecond RateLimitParameters `json:"per_second"`
}

type AccountRateLimits struct {
	Account RateLimitTypes `json:"account"`
	Device  RateLimitTypes `json:"device"`
}

type DeviceRateLimits struct {
	PerMinute RateLimitParameters `json:"per_minute"`
	PerSecond RateLimitParameters `json:"per_second"`
}

//small helper function for a faster transformation
func shrinker(inp, div int16) int16 {
	var outp int16
	if math.Round(float64(inp/div)) < 1 {
		outp = 1
	} else {
		outp = int16(math.Round(float64(inp / div)))
	}
	return outp
}

//NewRateLimits takes parameters and returns
//a new structure which holds all rate limits params
//It defines properties this way: you pass 3 params at the top
//for an Account minute restrictions, then we make in 60 times less
//for second restrictions. For a Device previous values are 5 times even less.
//So, we just boilerplate it. It's your responsibility to set proper values!!!
func NewAccountRateLimits(Reg, Inv, Total int16) *AccountRateLimits {
	//WE use simple strategy: we put passed params to
	//Minute type an 10 times less for a Second param
	accMinLimitParams := RateLimitParameters{
		Registrations: Reg,
		Invites:       Inv,
		TotalPackets:  Total,
	}

	var accReg, accInv, accTotal int16

	accReg = shrinker(Reg, 60)
	accInv = shrinker(Inv, 60)
	accTotal = shrinker(Total, 60)

	accSecLimitParams := RateLimitParameters{
		Registrations: accReg,
		Invites:       accInv,
		TotalPackets:  accTotal,
	}

	var devMinReg, devMinInv, devMinTotal, devSecReg, devSecInv, devSecTotal int16
	devMinReg = shrinker(Reg, 5)
	devMinInv = shrinker(Inv, 5)
	devMinTotal = shrinker(Total, 5)

	devSecReg = shrinker(devMinReg, 60)
	devSecInv = shrinker(devMinInv, 60)
	devSecTotal = shrinker(devMinTotal, 60)

	devMinLimitParams := RateLimitParameters{
		Registrations: devMinReg,
		Invites:       devMinInv,
		TotalPackets:  devMinTotal,
	}

	devSecLimitParams := RateLimitParameters{
		Registrations: devSecReg,
		Invites:       devSecInv,
		TotalPackets:  devSecTotal,
	}

	rateLimits := AccountRateLimits{
		Account: RateLimitTypes{
			PerMinute: accMinLimitParams,
			PerSecond: accSecLimitParams,
		},
		Device: RateLimitTypes{
			PerMinute: devMinLimitParams,
			PerSecond: devSecLimitParams,
		},
	}

	return &rateLimits
}

func NewDeviceRateLimits(Reg, Inv, Total int16) *DeviceRateLimits {
	//WE use simple strategy: we put passed params to
	//Minute type an 10 times less for a Second param
	minLimitParams := RateLimitParameters{
		Registrations: Reg,
		Invites:       Inv,
		TotalPackets:  Total,
	}

	var secReg, secInv, secTotal int16

	secReg = shrinker(Reg, 60)
	secInv = shrinker(Inv, 60)
	secTotal = shrinker(Total, 60)

	secLimitParams := RateLimitParameters{
		Registrations: secReg,
		Invites:       secInv,
		TotalPackets:  secTotal,
	}

	rateLimits := DeviceRateLimits{
		PerMinute: minLimitParams,
		PerSecond: secLimitParams,
	}

	return &rateLimits
}

func (r *RateLimiterService) CreateAccountRateLimits(
	ctx context.Context,
	account string,
	ratelimits *AccountRateLimits) (*http.Response, error) {

	var (
		localVarHttpMethod = "POST"
		localVarPostBody   interface{}
		localVarFileName   string
		localVarFileBytes  []byte
	)

	localVarHeaderParams := make(map[string]string)
	localVarQueryParams := url.Values{}
	localVarFormParams := url.Values{}

	// to determine the Content-Type header
	localVarHttpContentTypes := []string{}

	// set Content-Type header
	localVarHttpContentType := selectHeaderContentType(localVarHttpContentTypes)
	if localVarHttpContentType != "" {
		localVarHeaderParams["Content-Type"] = localVarHttpContentType
	}

	// to determine the Accept header
	localVarHttpHeaderAccepts := []string{
		"application/json",
	}

	// set Accept header
	localVarHttpHeaderAccept := selectHeaderAccept(localVarHttpHeaderAccepts)
	if localVarHttpHeaderAccept != "" {
		localVarHeaderParams["Accept"] = localVarHttpHeaderAccept
	}

	localVarPath := r.client.cfg.BasePath + "/accounts/" + account + "/rate_limits"

	data := RequestEnvelope{Data: ratelimits}

	localVarPostBody, err := setBody(data, "application/json")
	if err != nil {
		return nil, err
	}

	req, err := r.client.prepareRequest(ctx, localVarPath, localVarHttpMethod, localVarPostBody, localVarHeaderParams, localVarQueryParams, localVarFormParams, localVarFileName, localVarFileBytes)
	if err != nil {
		return nil, err
	}

	localVarHttpResponse, err := r.client.callAPI(ctx, req)
	if err != nil || localVarHttpResponse == nil {
		return localVarHttpResponse, err
	}
	defer localVarHttpResponse.Body.Close()

	if localVarHttpResponse.StatusCode >= 300 {
		bodyBytes, _ := ioutil.ReadAll(localVarHttpResponse.Body)
		return nil, reportError("Status: %v, Body: %s", localVarHttpResponse.Status, bodyBytes)
	}

	return localVarHttpResponse, err
}
