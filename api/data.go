package yetiapi

//RequestEnvelope is the main structure which must be presented
//for each request contains body: POST,PUT
type RequestEnvelope struct {
	Data      interface{} `json:"data"`
	AuthToken string      `json:"auth_token,omitempty"` //optional
	Verb      string      `json:"verb,omitempty"`       //optional
}

//ResponseEnvelope is the main structure which must be presented
//for each request contains body: POST,PUT
type ResponseEnvelope struct {
	Data interface{} `json:"data"`
}
