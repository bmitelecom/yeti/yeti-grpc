//Package yeti implements Yeti Switch project API
//and might be used for variety of operations.

package yetiapi

import (
	"bytes"
	"context"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"strconv"
	"strings"
	"time"

	"golang.org/x/oauth2"
)

var (
	API_URL = "http://localhost:6666/api/rest/admin"
	TOKEN   = ""
)

var (
	jsonCheck = regexp.MustCompile("(?i:[application|text]/json)")
	xmlCheck  = regexp.MustCompile("(?i:[application|text]/xml)")
)

// APIClient manages communication with a Kazoo API server
// In most cases there should be only one, shared, APIClient.
type APIClient struct {
	cfg    *Configuration
	common service // Reuse a single struct instead of allocating one for each service on the heap.

	// API Services
	DialpeerAPI *DialpeerApiService
	//RateLimitsAPI *RateLimiterService
	//PetApi   *PetApiService
	//StoreApi *StoreApiService
	//UserApi *UserApiService
}

type service struct {
	client *APIClient
}

// NewAPIClient creates a new API client.
//Requieres an API key or a set of username/password credentials
//Requires a userAgent string describing your application.
// optionally a custom http.Client to allow for advanced features such as caching.
func NewAPIClient(cfg *Configuration) (*APIClient, error) {
	if cfg.APIKey == nil {
		//Checking BasicAuth then
		if cfg.BasicAuth == nil {
			return nil, reportError("You have to specify an API key or username/password")
		}
	}
	if cfg.HTTPClient == nil {
		cfg.HTTPClient = http.DefaultClient
		cfg.HTTPClient.Timeout = time.Second * 5
	}

	c := &APIClient{}
	c.cfg = cfg
	c.common.client = c

	// API Services
	c.DialpeerAPI = (*DialpeerApiService)(&c.common)
	//c.RateLimitsAPI = (*RateLimiterService)(&c.common)
	//c.StoreApi = (*StoreApiService)(&c.common)
	//c.UserApi = (*UserApiService)(&c.common)

	return c, nil
}

func atoi(in string) (int, error) {
	return strconv.Atoi(in)
}

// selectHeaderContentType select a content type from the available list.
func selectHeaderContentType(contentTypes []string) string {
	if len(contentTypes) == 0 {
		return ""
	}
	if contains(contentTypes, "application/json") {
		return "application/json"
	}
	return contentTypes[0] // use the first content type specified in 'consumes'
}

// selectHeaderAccept join all accept types and return
func selectHeaderAccept(accepts []string) string {
	if len(accepts) == 0 {
		return ""
	}

	if contains(accepts, "application/json") {
		return "application/json"
	}

	return strings.Join(accepts, ",")
}

// contains is a case insenstive match, finding needle in a haystack
func contains(haystack []string, needle string) bool {
	for _, a := range haystack {
		if strings.ToLower(a) == strings.ToLower(needle) {
			return true
		}
	}
	return false
}

func readBody(resp *http.Response, v interface{}) error {
	if err := json.NewDecoder(resp.Body).Decode(&v); err != nil {
		resp.Body.Close()
		return err
	}
	return resp.Body.Close()
}

func path(segments ...string) string {
	r := ""
	for _, seg := range segments {
		r += "/"
		r += url.QueryEscape(seg)
	}
	return r
}

/*
// callAPI do the request.
func (c *APIClient) callAPI(request *http.Request) (*http.Response, error) {
	if !checkAuthState() {
		if err := c.Authenticate(ctx); err != nil {
			return nil, err
		} else {
			localVarRequest.Header.Add("X-Auth-Token", TOKEN)
		}
	}

	return c.cfg.HTTPClient.Do(request)
}
*/

// callAPI do the request.
func (c *APIClient) callAPI(ctx context.Context, request *http.Request) (*http.Response, error) {
	var (
		resp *http.Response
		err  error
	)

	if !checkAuthState() {
		if err := c.Authenticate(ctx); err != nil {
			return nil, err
		} else {
			request.Header.Add("Authorization", TOKEN)
			resp, err = c.cfg.HTTPClient.Do(request)
		}
	} else {
		request.Header.Add("Authorization", TOKEN)
		resp, err = c.cfg.HTTPClient.Do(request)
	}

	//We catch 401 response here and provide an authentication here
	if resp.StatusCode == 401 {
		go c.Authenticate(ctx)
		request.Header.Add("Authorization", TOKEN)
		resp, err = c.cfg.HTTPClient.Do(request)
		if err != nil {
			return nil, err
		}
	}
	return resp, nil
}

// Change base path to allow switching to mocks
func (c *APIClient) ChangeBasePath(path string) {
	c.cfg.BasePath = path
}

//simpleRequest we use when it's not necessary to use
//body of query params
func (c *APIClient) simpleRequest(
	ctx context.Context,
	path string,
	method string,
) (localVarRequest *http.Request, err error) {
	// Setup path and query parameters
	url, err := url.Parse(path)
	if err != nil {
		return nil, err
	}

	localVarRequest, err = http.NewRequest(method, url.String(), nil)
	if err != nil {
		return nil, err
	}

	// Override request host, if applicable
	if c.cfg.Host != "" {
		localVarRequest.Host = c.cfg.Host
	}

	// Add the user agent to the request.
	localVarRequest.Header.Add("User-Agent", c.cfg.UserAgent)

	if ctx != nil {
		// add context to the request
		localVarRequest = localVarRequest.WithContext(ctx)

		// Walk through any authentication.

		// OAuth2 authentication
		if tok, ok := ctx.Value(ContextOAuth2).(oauth2.TokenSource); ok {
			// We were able to grab an oauth2 token from the context
			var latestToken *oauth2.Token
			if latestToken, err = tok.Token(); err != nil {
				return nil, err
			}

			latestToken.SetAuthHeader(localVarRequest)
		}

		// Basic HTTP Authentication
		if auth, ok := ctx.Value(ContextBasicAuth).(BasicAuth); ok {
			localVarRequest.SetBasicAuth(auth.UserName, auth.Password)
		}

		// AccessToken Authentication
		if auth, ok := ctx.Value(ContextAccessToken).(string); ok {
			localVarRequest.Header.Add("Authorization", "Bearer "+auth)
		}
	}

	for header, value := range c.cfg.DefaultHeader {
		localVarRequest.Header.Add(header, value)
	}

	return localVarRequest, nil
}

// prepareRequest build the request
func (c *APIClient) prepareRequest(
	ctx context.Context,
	path string,
	method string,
	postBody interface{},
	headerParams map[string]string,
	queryParams url.Values,
	formParams url.Values,
	fileName string,
	fileBytes []byte) (localVarRequest *http.Request, err error) {

	var body *bytes.Buffer

	// Detect postBody type and post.
	if postBody != nil {
		contentType := headerParams["Content-Type"]
		if contentType == "" {
			contentType = detectContentType(postBody)
			headerParams["Content-Type"] = contentType
		}

		body, err = setBody(postBody, contentType)
		if err != nil {
			return nil, err
		}
	}

	// add form parameters and file if available.
	if len(formParams) > 0 || (len(fileBytes) > 0 && fileName != "") {
		if body != nil {
			return nil, errors.New("Cannot specify postBody and multipart form at the same time!")
		}
		body = &bytes.Buffer{}
		w := multipart.NewWriter(body)

		for k, v := range formParams {
			for _, iv := range v {
				if strings.HasPrefix(k, "@") { // file
					err = addFile(w, k[1:], iv)
					if err != nil {
						return nil, err
					}
				} else { // form value
					w.WriteField(k, iv)
				}
			}
		}
		if len(fileBytes) > 0 && fileName != "" {
			w.Boundary()
			//_, fileNm := filepath.Split(fileName)
			part, err := w.CreateFormFile("file", filepath.Base(fileName))
			if err != nil {
				return nil, err
			}
			_, err = part.Write(fileBytes)
			if err != nil {
				return nil, err
			}
			// Set the Boundary in the Content-Type
			headerParams["Content-Type"] = w.FormDataContentType()
		}

		// Set Content-Length
		headerParams["Content-Length"] = fmt.Sprintf("%d", body.Len())
		w.Close()
	}

	// Setup path and query parameters
	url, err := url.Parse(path)
	if err != nil {
		return nil, err
	}

	// Adding Query Param
	query := url.Query()
	for k, v := range queryParams {
		for _, iv := range v {
			query.Add(k, iv)
		}
	}

	// Encode the parameters.
	url.RawQuery = query.Encode()

	// Generate a new request
	if body != nil {
		localVarRequest, err = http.NewRequest(method, url.String(), body)
	} else {
		localVarRequest, err = http.NewRequest(method, url.String(), nil)
	}
	if err != nil {
		return nil, err
	}

	// add header parameters, if any
	if len(headerParams) > 0 {
		headers := http.Header{}
		for h, v := range headerParams {
			headers.Set(h, v)
		}
		localVarRequest.Header = headers
	}

	// Override request host, if applicable
	if c.cfg.Host != "" {
		localVarRequest.Host = c.cfg.Host
	}

	// Add the user agent to the request.
	localVarRequest.Header.Add("User-Agent", c.cfg.UserAgent)

	if ctx != nil {
		// add context to the request
		localVarRequest = localVarRequest.WithContext(ctx)

		// Walk through any authentication.

		// OAuth2 authentication
		if tok, ok := ctx.Value(ContextOAuth2).(oauth2.TokenSource); ok {
			// We were able to grab an oauth2 token from the context
			var latestToken *oauth2.Token
			if latestToken, err = tok.Token(); err != nil {
				return nil, err
			}

			latestToken.SetAuthHeader(localVarRequest)
		}

		// Basic HTTP Authentication
		if auth, ok := ctx.Value(ContextBasicAuth).(BasicAuth); ok {
			localVarRequest.SetBasicAuth(auth.UserName, auth.Password)
		}

		// AccessToken Authentication
		if auth, ok := ctx.Value(ContextAccessToken).(string); ok {
			localVarRequest.Header.Add("Authorization", "Bearer "+auth)
		}
	}

	for header, value := range c.cfg.DefaultHeader {
		localVarRequest.Header.Add(header, value)
	}

	return localVarRequest, nil
}

// Set request body from an interface{}
func setBody(body interface{}, contentType string) (bodyBuf *bytes.Buffer, err error) {
	if bodyBuf == nil {
		bodyBuf = &bytes.Buffer{}
	}

	if reader, ok := body.(io.Reader); ok {
		_, err = bodyBuf.ReadFrom(reader)
	} else if b, ok := body.([]byte); ok {
		_, err = bodyBuf.Write(b)
	} else if s, ok := body.(string); ok {
		_, err = bodyBuf.WriteString(s)
	} else if jsonCheck.MatchString(contentType) {
		err = json.NewEncoder(bodyBuf).Encode(body)
	} else if xmlCheck.MatchString(contentType) {
		xml.NewEncoder(bodyBuf).Encode(body)
	}

	if err != nil {
		return nil, err
	}

	if bodyBuf.Len() == 0 {
		err = fmt.Errorf("Invalid body type %s\n", contentType)
		return nil, err
	}
	return bodyBuf, nil
}

// detectContentType method is used to figure out `Request.Body` content type for request header
func detectContentType(body interface{}) string {
	contentType := "text/plain; charset=utf-8"
	kind := reflect.TypeOf(body).Kind()

	switch kind {
	case reflect.Struct, reflect.Map, reflect.Ptr:
		contentType = "application/json; charset=utf-8"
	case reflect.String:
		contentType = "text/plain; charset=utf-8"
	default:
		if b, ok := body.([]byte); ok {
			contentType = http.DetectContentType(b)
		} else if kind == reflect.Slice {
			contentType = "application/json; charset=utf-8"
		}
	}

	return contentType
}

// Prevent trying to import "fmt"
func reportError(format string, a ...interface{}) error {
	return fmt.Errorf(format, a...)
}

// Add a file to the multipart request
func addFile(w *multipart.Writer, fieldName, path string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	part, err := w.CreateFormFile(fieldName, filepath.Base(path))
	if err != nil {
		return err
	}
	_, err = io.Copy(part, file)

	return err
}

//GetMD5Hash is used when we use BasicAuth
func GetMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

//Authenticate provides an authentication on a
//Kazoo API server for bot api_key and password
//authentication methods
func (c *APIClient) Authenticate(ctx context.Context) error {
	var (
		localVarHTTPMethod = "POST"
		//localVarPostBody   interface{}
		localVarFileName  string
		localVarFileBytes []byte
		//rawAuthData        RequestEnvelope
	)

	if c.cfg.APIKey == nil && c.cfg.BasicAuth == nil {
		err := reportError("You must provide a valid API key or credentials for a basic auth")
		return err
	}

	if c.cfg.BasicAuth != nil {
		//We're going to use BasicAuth
		ba := c.cfg.BasicAuth
		if ba.UserName == "" || ba.Password == "" {
			err := reportError("You have to provide valid credentials: username and password")
			return err
		}

		//An envelope for auth data
		type authData struct {
			Data interface{} `json:"auth"`
		}

		authBody, err := json.Marshal(authData{Data: ba})
		if err != nil {
			return err
		}

		localVarPostBody, bodyErr := setBody(authBody, "json")
		if bodyErr != nil {
			return bodyErr
		}

		// create path and map variables
		localVarPath := c.common.client.cfg.BasePath + "/auth"

		localVarHeaderParams := make(map[string]string)
		localVarQueryParams := url.Values{}
		localVarFormParams := url.Values{}

		// body params
		//localVarPostBody = &authBody
		r, err := c.common.client.prepareRequest(
			ctx,
			localVarPath,
			localVarHTTPMethod,
			localVarPostBody,
			localVarHeaderParams,
			localVarQueryParams,
			localVarFormParams,
			localVarFileName,
			localVarFileBytes)

		if err != nil {
			return err
		}

		//localVarHTTPResponse, err := c.common.client.callAPI(ctx, r)
		localVarHTTPResponse, err := c.cfg.HTTPClient.Do(r)
		if err != nil || localVarHTTPResponse == nil {
			return err
		}
		defer localVarHTTPResponse.Body.Close()

		switch localVarHTTPResponse.StatusCode {
		case 201:
			//Succesfull authentication
			authdata := AuthResponse{}
			readBody(localVarHTTPResponse, &authdata)

			TOKEN = authdata.AuthToken
			go authTokenExpire()

			return nil

		default:
			bodyBytes, _ := ioutil.ReadAll(localVarHTTPResponse.Body)
			return reportError("Unsuccesfull attempt of authorization: %v, Body: %s", localVarHTTPResponse.Status, bodyBytes)
			//}
		}
	}
	return nil
}

func authTokenExpire() {
	timer := time.NewTimer(60 * time.Second)
	<-timer.C
	TOKEN = ""
}

func checkAuthState() bool {
	if len(TOKEN) == 0 {
		return false
	}
	return true
}
