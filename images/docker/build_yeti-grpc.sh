#!/bin/bash -l

set -e
whereis go
git clone -b $BRANCH --single-branch --depth 1 \
    $GIT_URL yeti-grpc
pushd $_
	#log::m-info "Applying patches ..."
	/usr/local/go/bin/go mod tidy
	/usr/local/go/bin/go env
	/usr/local/go/bin/go build

	chmod +x yeti-grpc

echo $PWD
