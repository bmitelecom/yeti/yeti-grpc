package main

import (
	"context"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"gitlab.com/bmitelecom/yeti/yeti-grpc/api"
	proto "gitlab.com/bmitelecom/yeti/yeti-api-protobuf"
	"github.com/golang/protobuf/ptypes/empty"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	yaml "gopkg.in/yaml.v2"
)

var yeti yetiapi.APIClient

var (
	Version    string
	Buildstamp string
)

var (
	config Configuration
	//Trace is necessary for debugging purposes
	Trace *log.Logger
	//Info corresponds to INFO syslog level
	Info *log.Logger
	//Warning corresponds to WARN syslog level
	Warning *log.Logger
	//Error corresponds to ERR syslog level
	Error *log.Logger
)

func init() {
	//Set up logging options
	traceHandle := ioutil.Discard
	infoHandle := os.Stdout
	warningHandle := os.Stdout
	errorHandle := os.Stderr

	Trace = log.New(traceHandle,
		"TRACE: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Info = log.New(infoHandle,
		"INFO: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Warning = log.New(warningHandle,
		"WARNING: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Error = log.New(errorHandle,
		"ERROR: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	//Loads the configuration from a file
	loadConfig()
}

//Configuration describes configuration params
type Configuration struct {
	Address         string   `yaml:"address"`
	ReadTimeout     int64    `yaml:"readTimeout"`
	WriteTimeout    int64    `yaml:"writeTimeout"`
	AdminUsers      []string `yaml:"adminUsers"`
	YetiAPIURL      string   `yaml:"yetiApiUrl"`
	YetiAPIUser     string   `yaml:"yetiApiUser"`
	YetiAPIPassword string   `yaml:"yetiApiPassword"`
	ContextTimeout  int64    `yaml:"contextTimeout"`
}

func loadConfig() {
	file, err := os.Open("config.yaml")
	if err != nil {
		Error.Printf("Cannot open config file: %s", err)
	}
	decoder := yaml.NewDecoder(file)
	config = Configuration{}
	err = decoder.Decode(&config)
	if err != nil {
		Error.Fatalf("Cannot get configuration from file: %s", err)
	}

	defer file.Close()

	//If environment variables are set, we use them instead of parameters in config.

	if v, s := os.LookupEnv("GRPC_ADDRESS"); s != false {
		if v != "" {
			config.Address = v
		}
	}

	if v, s := os.LookupEnv("YETI_API_URL"); s != false {
		if v != "" {
			config.YetiAPIURL = v
		}
	}

	if v, s := os.LookupEnv("YETI_API_USER"); s != false {
		if v != "" {
			config.YetiAPIUser = v
		}
	}

	if v, s := os.LookupEnv("YETI_API_PASSWORD"); s != false {
		if v != "" {
			config.YetiAPIPassword = v
		}
	}

	if v, s := os.LookupEnv("GRPC_READ_TIMEOUT"); s != false {
		if v != "" {
			d, err := strconv.Atoi(v)
			if err != nil {
				Info.Printf("Can't use value %s from variable %s: %s", v, "GRPC_READ_TIMEOUT", err)
			} else {
				config.ReadTimeout = int64(d)
			}

		}
	}

	if v, s := os.LookupEnv("GRPC_WRITE_TIMEOUT"); s != false {
		if v != "" {
			d, err := strconv.Atoi(v)
			if err != nil {
				Info.Printf("Can't use value %s from variable %s: %s", v, "GRPC_WRITE_TIMEOUT", err)
			} else {
				config.WriteTimeout = int64(d)
			}

		}
	}

	if v, s := os.LookupEnv("GRPC_CONTEXT_TIMEOUT"); s != false {
		if v != "" {
			d, err := strconv.Atoi(v)
			if err != nil {
				Info.Printf("Can't use value %s from variable %s: %s", v, "GRPC_CONTEXT_TIMEOUT", err)
			} else {
				config.ReadTimeout = int64(d)
			}

		}
	}

	Info.Printf("%+v\n", config)

}

// server is used to implement Yeti gRPC server.
type server struct{}

func (s *server) GetDialpeerByID(ctx context.Context, d *proto.ExistingDialpeer) (edp *proto.ExistingDialpeer, err error) {
	id := d.Id

	edp, err = yeti.DialpeerAPI.GetDialpeerByID(ctx, id)
	if err != nil {
		return nil, err
	}

	return edp, nil
}

func (s *server) GetDialpeerByFilter(filt *proto.Filters, stream proto.Yeti_GetDialpeerByFilterServer) error {
	ctx := context.Background()
	list, err := yeti.DialpeerAPI.ListDialpeersByFilter(ctx, filt)
	if err != nil {
		Error.Println(err)
		return err
	}

	for _, dp := range list {
		if err := stream.Send(dp); err != nil {
			Error.Println(err)
			return err
		}
	}

	return nil
}

func (s *server) CreateDialpeer(ctx context.Context, d *proto.Dialpeer) (edp *proto.ExistingDialpeer, err error) {
	edp, err = yeti.DialpeerAPI.CreateDialpeer(ctx, d)
	if err != nil {
		Error.Println(err)
		return nil, err
	}
	return edp, nil
}

func (s *server) UpdateDialpeer(ctx context.Context, d *proto.ExistingDialpeer) (edp *proto.ExistingDialpeer, err error) {
	edp, err = yeti.DialpeerAPI.UpdateDialpeer(ctx, d)
	if err != nil {
		return nil, err
	}

	return edp, nil
}

func (s *server) CreateDialpeerNextRate(ctx context.Context, r *proto.DialpeerNextRate) (nr *proto.DialpeerNextRate, err error) {
	
	return nr, nil
}

func (s *server) UpdateDialpeerNextRate(ctx context.Context, r *proto.DialpeerNextRate) (nr *proto.DialpeerNextRate, err error) {
	
	return nr, nil
}

func (s *server) DeleteDialpeerNextRate(ctx context.Context, r *proto.DialpeerNextRate) (nr *proto.DialpeerNextRate, err error) {
	
	return nr, nil
}

func (s *server) GetDialpeerNextRate(ctx context.Context, r *proto.DialpeerNextRate) (nr *proto.DialpeerNextRate, err error) {
	
	return nr, nil
}

func (s *server) ListDialpeerNextRate( r *proto.DialpeerNextRate, stream proto.Yeti_ListDialpeerNextRateServer) error {
	
	return nil
}

func (s *server) ShowVersion(ctx context.Context, x *empty.Empty) (ver *proto.Version, err error) {
	ver.Id = Version
	Info.Print(ver)
	return ver, nil
}

//healthz endpoint is necessary for Docker and Kubernetes checks
func healthz(writer http.ResponseWriter, request *http.Request) {
	writer.Write([]byte("200 - It works!"))
}

func main() {
	/*
		// handle static assets
		mux := http.NewServeMux()

		// Health requests from K8S
		mux.HandleFunc("/healthz", healthz)

		// starting up the server
		server := &http.Server{
			Addr:           config.Address,
			Handler:        mux,
			ReadTimeout:    time.Duration(config.ReadTimeout * int64(time.Second)),
			WriteTimeout:   time.Duration(config.WriteTimeout * int64(time.Second)),
			MaxHeaderBytes: 1 << 20,
		}*/

	cfg := yetiapi.NewConfiguration()
	cfg.BasePath = config.YetiAPIURL

	auth := yetiapi.BasicAuth{UserName: config.YetiAPIUser, Password: config.YetiAPIPassword}
	cfg.BasicAuth = &auth

	clt, err := yetiapi.NewAPIClient(cfg)
	if err != nil {
		Error.Fatalf("failed to serve: %v", err)
	}
	yeti = *clt

	lis, err := net.Listen("tcp", config.Address)
	if err != nil {
		Error.Fatalf("failed to listen: %v", err)
	}

	errHandler := func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (interface{}, error) {
		resp, err := handler(ctx, req)

		if err != nil {
			Error.Printf("Server: %s | Method %q Failed: %s", info.FullMethod, err)
		} else {
			Info.Printf("Server: %s | Method %q", info.Server, info.FullMethod)
		}
		return resp, err
	}

	s := grpc.NewServer(grpc.UnaryInterceptor(errHandler))

	proto.RegisterYetiServer(s, &server{})
	// Register reflection service on gRPC server.w
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		Error.Fatalf("failed to serve: %v", err)
	}
}
