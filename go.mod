module gitlab.com/bmitelecom/yeti/yeti-grpc

require (
	github.com/golang/protobuf v1.2.0
	github.com/kr/pretty v0.1.0 // indirect
	gitlab.com/bmitelecom/yeti/yeti-api-protobuf v0.1.0
	golang.org/x/net v0.0.0-20181011144130-49bb7cea24b1
	golang.org/x/oauth2 v0.0.0-20181003184128-c57b0facaced
	golang.org/x/sys v0.0.0-20181011152604-fa43e7bc11ba // indirect
	google.golang.org/appengine v1.2.0 // indirect
	google.golang.org/genproto v0.0.0-20181004005441-af9cb2a35e7f // indirect
	google.golang.org/grpc v1.15.0
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.1
)
